import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/map';

import { Items } from '../../data/items';

@Injectable()
export class SearchService {

  constructor(private http: Http) { }

  public itemsArray = new Subject<Items[]>();
  tempArray = [];

  //HTTP GET returns array of items
  listItems() {
    return this.http
      .get('assets/data/items.json')
      .map((response) => response.json())
      .toPromise();
  }

  //UPDATE itemsArray
  updateItems(item) {
    this.itemsArray.next(item);
  }

}
