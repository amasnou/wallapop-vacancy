import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';

@Injectable()
export class FavouritesService {

  public modalStatus = new Subject<boolean>();

  constructor() { }

  switchModalStatus(status) {
    //SWITCH (open and close)favourites modal
    this.modalStatus.next(status);
  }

}
