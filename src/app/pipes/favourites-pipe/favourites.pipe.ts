import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'favourites'
})
export class FavouritesPipe implements PipeTransform {

  transform(items: any[]): any[] {
    if (!items) return [];
    return items.filter(it => {
      return it.favourite == true;
    });
  }

}
