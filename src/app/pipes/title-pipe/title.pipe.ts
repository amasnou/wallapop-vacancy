import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'titlePipe'
})
export class TitlePipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter(it => {
      return it.title.toLowerCase().includes(searchText) || it.description.toLowerCase().includes(searchText) || it.email.toLowerCase().includes(searchText) || it.price == searchText;
    });
  }
  
}