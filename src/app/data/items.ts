export interface Items {
    title: String;
    description: String;
    price: String;
    email: String;
    image: String;
}
