import { Component, OnInit } from '@angular/core';

import { FavouritesService } from '../services/favourites-service/favourites.service';
import { SearchService } from '../services/search-service/search.service';

import { FavouritesPipe } from '../pipes/favourites-pipe/favourites.pipe';

@Component({
  selector: 'app-walla-favourites',
  templateUrl: './walla-favourites.component.html',
  styleUrls: ['./walla-favourites.component.scss']
})
export class WallaFavouritesComponent implements OnInit {

  public modalStatus: boolean = false;
  public favItems: any[] = [];
  public searchResult: any[] = [];

  constructor(private _favouritesService: FavouritesService, private _searchService: SearchService, private favouritePipes: FavouritesPipe) {
    //SUBSCRIBE to modal status changes (open and close)
    this._favouritesService.modalStatus.subscribe(value => {
      this.modalStatus = value;
    })

    //SUBSCRIBE to changes in array of items in search service and filter for favourite items
    this._searchService.itemsArray.subscribe(items => {
      this.favItems = items;
      this.searchResult = items;
      this.favItems = this.favouritePipes.transform(this.favItems);
    })

  }

  ngOnInit() {
  }

  //CLOSE modal on click in background or X button
  toggleFavModal() {
    this._favouritesService.switchModalStatus(false);
  }

  //REMOVE item as favourite
  removeFavourite(item) {
    item.favourite = false;
    this._searchService.updateItems(this.searchResult);
  }

}
