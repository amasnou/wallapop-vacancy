import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallaFavouritesComponent } from './walla-favourites.component';

describe('WallaFavouritesComponent', () => {
  let component: WallaFavouritesComponent;
  let fixture: ComponentFixture<WallaFavouritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallaFavouritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallaFavouritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
