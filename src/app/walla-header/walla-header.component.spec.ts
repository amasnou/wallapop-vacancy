import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { WallaHeaderComponent } from './walla-header.component';
import { FavouritesService } from '../services/favourites-service/favourites.service';

describe('WallaHeaderComponent', () => {
  let component: WallaHeaderComponent;
  let fixture: ComponentFixture<WallaHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WallaHeaderComponent],
      providers: [FavouritesService],
      imports: [RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallaHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open favourites modal on click on favourites link', async(() => {
    spyOn(component, 'toggleFavModal');

    let tab = fixture.debugElement.nativeElement.querySelector('.WallaHeader__tab--favourites');
    tab.click();

    fixture.whenStable().then(() => {
      expect(component.toggleFavModal).toHaveBeenCalled();
    })
  }));

  it('should go home by clicking on Wallapop logo', async(() => {
    spyOn(component, 'toggleFavModal');

    let href = fixture.debugElement.nativeElement.querySelector('.WallaHeader__logo').getAttribute('href');

    expect(href).toEqual('/');

  }));

});
