import { Component, OnInit } from '@angular/core';

import { FavouritesService } from '../services/favourites-service/favourites.service';

@Component({
  selector: 'app-walla-header',
  templateUrl: './walla-header.component.html',
  styleUrls: ['./walla-header.component.scss']
})
export class WallaHeaderComponent implements OnInit {

  constructor(private _favouritesService: FavouritesService) { }

  ngOnInit() {
  }

  //OPEN favourites modal calling to favourites-service
  toggleFavModal() {
    this._favouritesService.switchModalStatus(true);
  }

}
