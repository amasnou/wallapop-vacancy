import { Component, OnInit, Input } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-walla-filterbar',
  templateUrl: './walla-filterbar.component.html',
  styleUrls: ['./walla-filterbar.component.scss']
})
export class WallaFilterbarComponent implements OnInit {

  @Input() search: string;
  public searchBtnActivated: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  //NAVIGATE to walla-results component passing search value as parameter
  makeSearch(searchValue) {
    this.router.navigate(['search', searchValue]);
  }

  //ACTIVATE or DESACTIVATE search button
  onSearchChange(searchValue: string) {
    if (searchValue != '')
      this.searchBtnActivated = true;
    else
      this.searchBtnActivated = false;
  }
}

