import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Location} from '@angular/common';

import { WallaFilterbarComponent } from './walla-filterbar.component';
import { WallaResultsComponent } from '../walla-results/walla-results.component';

describe('WallaFilterbarComponent', () => {
  let component: WallaFilterbarComponent;
  let fixture: ComponentFixture<WallaFilterbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WallaFilterbarComponent, WallaResultsComponent],
      imports: [RouterTestingModule, RouterTestingModule.withRoutes([
        { path: 'search/:item', component: WallaResultsComponent }
      ])]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallaFilterbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call makeSearch function when clicking on search button', () => {
    spyOn(component, 'makeSearch');

    let tab = fixture.debugElement.nativeElement.querySelector('.WallaFilter__searchbtn');
    tab.click();

    fixture.whenStable().then(() => {
      expect(component.makeSearch).toHaveBeenCalled();
    })

  });

  it('should go to url', async(inject([Router, Location], (router: Router, location: Location) => {

    let button = fixture.debugElement.nativeElement.querySelector('.WallaFilter__searchbtn');
    button.click();

    fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/search/iphone');
    })

  })));
});
