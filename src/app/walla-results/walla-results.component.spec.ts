import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallaResultsComponent } from './walla-results.component';

describe('WallaResultsComponent', () => {
  let component: WallaResultsComponent;
  let fixture: ComponentFixture<WallaResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallaResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallaResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
