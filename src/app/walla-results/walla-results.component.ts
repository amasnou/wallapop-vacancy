import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../services/search-service/search.service';

@Component({
  selector: 'app-walla-results',
  templateUrl: './walla-results.component.html',
  styleUrls: ['./walla-results.component.scss']
})
export class WallaResultsComponent implements OnInit {

  @Input() isResultsPage: boolean;
  public searchResult: any[] = [];
  private errorMessage: any = '';
  public searchString: string;
  public sortVar: string = 'price';
  public p: number = 1;

  constructor(private route: ActivatedRoute, private _searchService: SearchService) {
    //GET the search parameter and make the search with _searchService
    this.route.params.subscribe(res => this.execSearch(res.item));

    //SUBSCRIBE to changes in array of items in data service
    this._searchService.itemsArray.subscribe(value => {
      this.searchResult = value;
    })
  }

  ngOnInit() {
  }

  //CALL SearchService passing item to search as attribute
  execSearch(searchValue) {
    this.searchString = searchValue;
    this._searchService.listItems().then(result => {
      this.searchResult = result.items;
    });
  }

  //ADD item as favourite
  addFavourite(item) {
    if (item.favourite) {
      item.favourite = false;
    } else {
      item.favourite = true;
    }
    this._searchService.updateItems(this.searchResult);
  }

  //SET sort variable
  setSortVar(variable) {
    this.sortVar = variable;
  }

}
