import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallaSearchComponent } from './walla-search.component';

describe('WallaSearchComponent', () => {
  let component: WallaSearchComponent;
  let fixture: ComponentFixture<WallaSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallaSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallaSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
