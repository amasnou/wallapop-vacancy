import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WallaSearchComponent } from './walla-search/walla-search.component';
import { WallaResultsComponent } from './walla-results/walla-results.component';

export const routes: Routes = [
    {
        path: '',
        component: WallaSearchComponent,
        pathMatch: 'full'

    },
    {
        path: 'search',
        redirectTo: '/',
        pathMatch: 'full'
    },
    {
        path: 'search/:item',
        component: WallaResultsComponent
    }
];

export const RoutingModule: ModuleWithProviders = RouterModule.forRoot(routes);