import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RoutingModule } from './app.router';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { WallaHeaderComponent } from './walla-header/walla-header.component';
import { WallaSearchComponent } from './walla-search/walla-search.component';
import { WallaResultsComponent } from './walla-results/walla-results.component';
import { WallaFilterbarComponent } from './walla-filterbar/walla-filterbar.component';
import { WallaFavouritesComponent } from './walla-favourites/walla-favourites.component';

import { SearchService } from './services/search-service/search.service';
import { FavouritesService } from './services/favourites-service/favourites.service';

import { FavouritesPipe } from './pipes/favourites-pipe/favourites.pipe';
import { TitlePipe } from './pipes/title-pipe/title.pipe';
import { SortPipe } from './pipes/sort-pipe/sort.pipe';


@NgModule({
  declarations: [
    AppComponent,
    WallaHeaderComponent,
    WallaSearchComponent,
    WallaResultsComponent,
    WallaFilterbarComponent,
    WallaFavouritesComponent,
    FavouritesPipe,
    TitlePipe,
    SortPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RoutingModule,
    FormsModule,
    NgxPaginationModule
  ],
  providers: [SearchService, FavouritesService, FavouritesPipe, TitlePipe, SortPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
